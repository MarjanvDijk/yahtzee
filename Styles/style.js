import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FEFEDF",
  },
  gameboard: {
    backgroundColor: "#FEFEDF",
    paddingVertical: 15,
  },
  header: {
    marginTop: 30,
    backgroundColor: "#006E6A",
    flexDirection: "row",
  },
  footer: {
    marginBottom: 30,
    backgroundColor: "#006E6A",
    flexDirection: "row",
  },
  title: {
    color: "#fff",
    fontWeight: "bold",
    flex: 1,
    fontSize: 23,
    textAlign: "center",
    margin: 10,
  },
  author: {
    color: "#fff",
    fontWeight: "bold",
    flex: 1,
    fontSize: 15,
    textAlign: "center",
    margin: 10,
  },
  gameinfo: {
    textAlign: "center",
    justifyContent: "center",
    fontSize: 20,
    marginTop: 10,
  },
  bonusInfo: {
    textAlign: "center",
    justifyContent: "center",
    fontSize: 15,
    marginTop: 10,
  },
  flexItemSpots: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  flexBoxSpots: {
    margin: 10,
  },
  flexDice: {
    flexDirection: "row",
    justifyContent: "center",
  },
  button: {
    marginVertical: 30,
    padding: 10,
    backgroundColor: "#006E6A",
    width: 140,
    borderRadius: 15,
    alignSelf: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 20,
    alignSelf: "center",
    fontWeight: '400'
  },
});
