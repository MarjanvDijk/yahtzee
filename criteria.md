- [x] App works both in phone (Expo) and web browser 8
- [x] The structure of the files of the app is like in the Tic Tac Toe exercise 5
- [x] The user interface of the app follows the instructions (own styles are ok) 5
- [x] Game logic: Initialization of the app (new game) follows the instructions 8
- [x] Game logic: Throwing and selecting dices follow the instructions 8
- [x] Game logic: Calculating and showing points follow the instructions 8
- [x] Game logic: Handling abnormal situations follows the instructions 8

# Handling abnormal situations:
- [x] status: 'Throw 3 times before setting points' if user clicks on circles before having thrown 3 times
- [x] status: 'Set points before throwing dices' if user clicks on throw dices before selecting circle
- [x] status: 'You have already selected these points' if user clicks on circle that is already chosen
- [x] it does not matter if the dices are selected when selecting circle
- [x] user can not select dices before next throw after choosing circle
- [x] after game ends user can not select dices before throwing dices
