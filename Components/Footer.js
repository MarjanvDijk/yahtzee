import React from "react";
import { Text, View } from "react-native";
import styles from "../Styles/style";

export default function Footer() {
  return (
    <View style={styles.footer}>
      <Text style={styles.title}>Author: Marjan van Dijk</Text>
    </View>
  );
}
