import React, { useState, useEffect } from "react";
import { Text, View, Pressable } from "react-native";
import styles from "../Styles/style";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

let board = [];
const NBR_OF_DICES = 5;
const NBR_OF_THROWS = 3;
const BONUS_LIMIT = 63;
const BONUS_POINTS = 35;
let hasBonus = false;

/**
 * @author Marjan van Dijk
 * Gameboard shows the user the yahtzee playing field and allowes the user to interact with it
 */
export default function Gameboard() {
  const [select, setSelect] = useState(false);
  const [nbrOfThrowsLeft, setNbrOfThrowsLeft] = useState(NBR_OF_THROWS);
  const [status, setStatus] = useState("Throw dices to start the game");
  const [selectedDices, setSelectedDices] = useState(
    new Array(NBR_OF_DICES).fill(false)
  );
  const [score, setScore] = useState(0);
  const [selectedCircles, setSelectedCircles] = useState(
    new Array(6).fill(false)
  );
  const [circleNumbers, setCircleNumbers] = useState(new Array(6).fill(0));
  const [bonusText, setBonusText] = useState(
    "You are " + BONUS_LIMIT + " points away from the bonus"
  );

  /**
   * The useEffect will take place when the user clicks on a circle to set the points.
   * At that point the score should be calculated.
   * The dices are reset after the user has selected their points so that in the next throw non of the dices are selected
   * When all the circles are selected the game is over.
   * After 5 seconds of finishing the game, the game will be reset automatically
   */
  useEffect(() => {
    calculateTotalScore();
    if (nbrOfThrowsLeft === 3 && !select) {
      setSelectedDices(new Array(NBR_OF_DICES).fill(false));
    }
    if (selectedCircles.every((element) => element === true)) {
      setSelect(true);
      if (hasBonus) {
        setStatus(
          "Game over. All points are selected. You got the bonus :) The game will reset in 5 seconds"
        );
      } else {
        setStatus(
          "Game over. All points are selected. No bonus :( The game will reset in 5 seconds"
        );
      }
      setTimeout(function () {
        resetGame();
      }, 5000);
    }
  }, [selectedCircles]);

  /**
   * resetGame will reset all the states to be able to play the game again.
   */
  function resetGame() {
    setSelect(false);
    setNbrOfThrowsLeft(NBR_OF_THROWS);
    setStatus("Throw dices to start the game");
    setSelectedDices(new Array(NBR_OF_DICES).fill(false));
    setSelectedCircles(new Array(6).fill(false));
    setCircleNumbers(new Array(6).fill(0));
    setScore(0);
    setBonusText("You are " + BONUS_LIMIT + " points away from the bonus");
    hasBonus = false;
  }

  /**
   * Creation of the dices
   * The dices are all clickable and when pressed they are selected/unselected depending on their current state
   */
  const dicesRow = [];
  for (let i = 0; i < NBR_OF_DICES; i++) {
    dicesRow.push(
      <Pressable key={"dicesRow" + i} onPress={() => selectDice(i)}>
        <MaterialCommunityIcons
          name={board[i]}
          key={"dicesRow" + i}
          size={50}
          color={selectedDices[i] ? "#5F0071" : "#9B0057"}
        ></MaterialCommunityIcons>
      </Pressable>
    );
  }

  /**
   * Creation of the circles that allow the user to choose the points
   * The circles are all pressable and when pressed the circle will be selected.
   */
  const circleRow = [];
  for (let i = 0; i < 6; i++) {
    circleRow.push(
      <Pressable key={"circleRow" + i} onPress={() => selectCircle(i)}>
        <MaterialCommunityIcons
          name={"numeric-" + (i + 1) + "-circle"}
          key={"circleRow" + i}
          size={30}
          color={selectedCircles[i] ? "#5F0071" : "#9B0057"}
        ></MaterialCommunityIcons>
      </Pressable>
    );
  }

  /**
   * Creation of the numbers that show the spotscore above the circles to choose the points
   */
  const nrRow = [];
  for (let i = 0; i < circleNumbers.length; i++) {
    nrRow.push(<Text key={"circleNumber" + i}>{circleNumbers[i]}</Text>);
  }

  /**
   * selectDice will allow the user to select and deselect a dice.
   * It starts bij checking if the user is not cheating and selecting dices before they are thrown.
   * If the user is allowed to select dices they will change color and the status of the dice will be updated. Either true or false
   * @param diceNumber is the array index of the chosen dice
   */
  function selectDice(diceNumber) {
    if (nbrOfThrowsLeft !== 3) {
      let dices = [...selectedDices];
      dices[diceNumber] = selectedDices[diceNumber] ? false : true;
      setSelectedDices(dices);
    } else {
      setStatus("No cheating... Throw dices first");
    }
  }

  /**
   * selectCircle is a function that allowes the user to select a circle and choose points.
   * It starts by checking if the user has thown 3 times and therfore allowed to choose the points.
   *    If not allowed the status will update and nothing will happen.
   *
   *    If the user is allowed to select a circle it will check if the circle that the user wants to select is already selected.
   *        If that is the case the status will update and tell the user that they can not select it again
   *        If the user can select the circle will be selected and the score will be calculated and the user can continue the game
   * @param circleNumber is the array number of the chosen points.
   */
  function selectCircle(circleNumber) {
    if (select) {
      if (!selectedCircles[circleNumber]) {
        let circles = [...selectedCircles];
        circles[circleNumber] = true;

        calculateSpotScore(circleNumber + 1);
        setSelect(false);
        setNbrOfThrowsLeft(NBR_OF_THROWS);
        setStatus("Throw dices to continue");
        setSelectedCircles(circles);
      } else {
        setStatus(
          "You have already selected " + (circleNumber + 1) + " before"
        );
      }
    } else {
      setStatus("Throw 3 times before choosing your points");
    }
  }

  /**
   * throwDices starts of by checking is the user needs to select their points.
   *   If that is the case the status will be updated to tell the user and the dices will NOT be thrown
   *   If the user is allowed to throw the dices it will check what dices are not selected and it will randomly generate a dice.
   * Based on the number of throws left the status will update.
   */
  function throwDices() {
    if (!select) {
      for (let i = 0; i < NBR_OF_DICES; i++) {
        if (!selectedDices[i]) {
          let randomNumber = Math.floor(Math.random() * 6 + 1);
          board[i] = "dice-" + randomNumber;
        }
      }
      setNbrOfThrowsLeft(nbrOfThrowsLeft - 1);
      if (nbrOfThrowsLeft === 1) {
        setStatus("Choose your points");
        setSelect(true);
      } else {
        setStatus("Select dices and throw again");
      }
    } else {
      setStatus("Set points before throwing dices");
    }
  }

  /**
   * The function goes through the board (the dices) and checks if there is a dice with the given number. It counts the amount of dices there is.
   * Then it will multiply the amount of dices by the diceNumber in order to get the spotScore.
   * After that it sets the spotscore to show on the screen above the chosen number.
   * @param diceNumber is the number of the dice that is chosen.
   */
  function calculateSpotScore(diceNumber) {
    let count = 0;
    for (let i = 0; i < board.length; i++) {
      if (board[i].endsWith(diceNumber)) {
        count++;
      }
    }
    let spotscore = diceNumber * count;
    let tempCircleNumbers = [...circleNumbers];
    tempCircleNumbers[diceNumber - 1] = spotscore;
    setCircleNumbers(tempCircleNumbers);
  }

  /** This function counts all the scores of the spotscores and adds them up.
   * After it checks if the score is above the bonus limit (63). If zo it will add a bonus (35) to the total score.
   * It sets the text to inform the user how many points the user needs until they reach the bonus limit.
   */

  function calculateTotalScore() {
    let count = circleNumbers.reduce((a, b) => a + b, 0)
    if (count >= BONUS_LIMIT) {
      setScore(count + BONUS_POINTS);
      hasBonus = true;
      setBonusText("You received a bonus of " + BONUS_POINTS + " points");
    } else {
      setBonusText(
        "You are " + (BONUS_LIMIT - count) + " points away from the bonus"
      );
      setScore(count);
    }
  }

  return (
    <View style={styles.gameboard}>
      <View style={styles.flexDice}>{dicesRow}</View>
      <Text style={styles.gameinfo}>Throws left: {nbrOfThrowsLeft}</Text>
      <Text style={styles.gameinfo}>{status}</Text>
      <Pressable style={styles.button} onPress={() => throwDices()}>
        <Text style={styles.buttonText}>Throw dices</Text>
      </Pressable>
      <Text style={styles.gameinfo}>Total: {score}</Text>
      <Text style={styles.bonusInfo}>{bonusText}</Text>
      <View style={styles.flexBoxSpots}>
        <View style={styles.flexItemSpots}>{nrRow}</View>
        <View style={styles.flexItemSpots}>{circleRow}</View>
      </View>
    </View>
  );
}
