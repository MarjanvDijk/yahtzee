import React from "react";
import { View } from "react-native";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Gameboard from "./Components/Gameboard";
import styles from './Styles/style';

export default function App() {
  return (
    <View style={styles.container}>
      <Header />
      <Gameboard />
      <Footer />
    </View>
  );
}
